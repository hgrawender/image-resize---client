function getFileData(myFile){
            document.getElementById("welcome_text").style.display='none'
            var file = myFile.files[0];  
            var filename = file.name;
            document.getElementById("desiredFilename").value = filename;

            document.getElementById('imgPreview').src = window.URL.createObjectURL(file)
            document.getElementById('preview').style.display='inline'

            document.getElementById('btnSubmit').removeAttribute('disabled')
            if( file ) {
                    var img = new Image();
                    img.src = window.URL.createObjectURL( file );
                    img.onload = function() {
                        var width = img.naturalWidth,
                            height = img.naturalHeight;
                    document.getElementById("originalHeight").value = height;
                    document.getElementById("originalWidth").value = width;
                    document.getElementById("desiredHeight").value = height;
                    document.getElementById("desiredWidth").value = width;
                    };
                }
        }


function widthKeepRatio(){
        if(document.getElementById("ratio").checked == true){
                var oH = document.getElementById("originalHeight").value
                var oW = document.getElementById("originalWidth").value
                var dH = document.getElementById("desiredHeight").value
                document.getElementById("desiredWidth").value = Math.round ((dH * oW) / oH)
                }
        }

function heightKeepRatio(){
        if(document.getElementById("ratio").checked == true){
                var oH = document.getElementById("originalHeight").value
                var oW = document.getElementById("originalWidth").value
                var dW = document.getElementById("desiredWidth").value
                document.getElementById("desiredHeight").value = Math.round ((oH * dW) / oW)
                }
        }

$('.btn-number').click(function(e){
    e.preventDefault();
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());

    if (!isNaN(currentVal)) {
        if(type == 'minus') {
            if(currentVal>0){
                input.val(currentVal - 1).change();
            }
        } else if(type == 'plus') {
                input.val(currentVal + 1).change();
        }
    } else {
        input.val(0);
    }
});



function showImageModified(){
        document.getElementById("imageModified").style.display = "block"; 
        document.getElementById('showModified').style.display = "none";
        document.getElementById('imageModifiedDownload').style.display = "block"; 
};



//POPRAWIONE
 
var request;

$(document).ready(function () {

    $("#btnSubmit").click(function (event) {
        //stop submit the form
        event.preventDefault();
        var form = $('#uploadForm')[0];
        var data = new FormData(form);

        data.append("CustomField", "This is some extra data, testing");
        $("#btnSubmit").prop("disabled", true);

       $request = $.ajax({
            type: "POST",
             url: "http://localhost:8080/ResizeImageJSON",
            dataType: "json",

            enctype: 'multipart/form-data',
            // url: "http://localhost:8080/ResimageUpDBDown",
             data: data,
             processData: false,
             contentType: false,
             cache: false,
            // timeout: 600000,
            success: function (data) {

                //$("#result").text(data);
                //$("#result").html(data);
               
               // $('#imageModified1').html('<img src="http://localhost:8080/getImageModified/88" />');
             $('#imageModified').html('<br /><br /><img id="imgDownload" class="center" src="data:image/jpg;base64,'+data+'" />');
                $("#btnSubmit").prop("disabled", false);

            },
            error: function (e) {

                $("#result").text(e.responseText);
                console.log("ERROR : ", e);
                $("#btnSubmit").prop("disabled", false);

            }
        });
        document.getElementById('imageModified').style.display = "none"; 
        document.getElementById('showModified').style.display = "block"; 
        document.getElementById('imageModifiedDownload').style.display = "none";


    });

});


$(".deleteLink").click(function() {

     if (confirm("Are you sure about deleting it?")) {
        
        event.preventDefault();
          $.ajax({
              url: $(this).attr('href'),
              type: "get", //send it through get method
              data: { 
               
              },
              success: function(response) {
                location.reload();
              },
              error: function(xhr) {
                alert("Error");
              }
            });
    }
    return false;

});

function prepHref(linkElement) {
    var desiredFilename = document.getElementById('desiredFilename');
    var button = document.getElementById('btn-download');
    var myImage = document.getElementById('imgDownload');
    button.download = desiredFilename.value;
    linkElement.href = myImage.src;
}