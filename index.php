<!DOCTYPE html>
<html>


<head>
	<title>Resize image</title>
	<link rel="icon" href="ico.png">
	<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

	
	
	<link rel="stylesheet" type="text/css" href="layout.css">
	<!-- <script type="text/javascript" src="script.js"></script> -->

</head>
<body>
<div class="container"> <br />
    <div class="row">


    	<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading"><strong>Upload and resize images</strong> 
				<small> - by Hubert Grawender </small></div>
				<div class="panel-body">
				
				<form id="uploadForm" action="localhost:8080/ResimageUpDBDown" method="POST" enctype="multipart/form-data">
					<div class="input-group image-preview">
						<input placeholder="" type="text" name="desiredFilename" id="desiredFilename" class="form-control image-preview-filename" >
						<!-- don't give a name === doesn't send on POST/GET --> 
						<span class="input-group-btn"> 


						<label class="btn btn-primary btn-file">
						<span class="btn-label"><i class="glyphicon glyphicon-folder-open"></i> </span>
   						 Browse <input type="file" name="file" style="display: none;" onchange="getFileData(this);" >
						</label>

						<button type="submit" id="btnSubmit" class="btn btn-labeled btn-success" disabled> <span class="btn-label"><i class="glyphicon glyphicon-upload"></i> </span>Upload</button>
						</span> 



					</div><br>
					<strong  class="center" id="welcome_text">Please upload image first!</strong>
					<div class="col-md-12"  id='preview' style="display: none; padding-left: 0px;">
						
						<strong>Image to be uploaded:</strong><br/><br/>
						<div class="col-md-2"></div>
						<div class="col-md-6">
						<img  id="imgPreview" src="#" class="center" style="width: auto; max-height: 150px;" />
						</div>
						<div class="col-md-3">
							<div  class="input-group mb-3">
							  <div class="input-group-prepend">
							    <span class="input-group-text" id="basic-addon1">Orignal height</span>
							  </div>
							  <input id="originalHeight" type="text" value="1" class="form-control" disabled>
							</div><br/>
						
							<div  class="input-group mb-3">
							  <div class="input-group-prepend">
							    <span class="input-group-text" id="basic-addon1">Orignal width</span>
							  </div>
							  <input id="originalWidth" type="text"  value="1"class="form-control" disabled>
							</div>
						</div>
						<div class="col-md-1"></div>
						<br>
					</div>


					<br>
					<div class="col-md-12"><hr style="height: 4px;"> <strong>&nbsp;Available options:</strong></div>
					<br />
					<div class="col-md-4 col-md-offset-2">
					<br />
						<input type="checkbox" name="mirror"> Mirror<br><br>
						<input type="checkbox" id="ratio"> Keep aspect ratio<br>
					</div>
						<div class="col-md-6">

						 <div class="input-group">
    						<span class="input-group-addon"><i class="glyphicon glyphicon-resize-vertical"></i> &nbsp;Desired height [px]</span>
    						<input name="desiredHeight" id="desiredHeight" type="text" class="form-control input-number" onchange="widthKeepRatio();">
    						 <span class="input-group-btn">
					              <button  type="button" class="btn btn-default btn-number" data-type="plus" data-field="desiredHeight">
					                  <span class="glyphicon glyphicon-plus"></span>
					              </button>
					          </span>
    						 <span class="input-group-btn">
					              <button  type="button" class="btn btn-default btn-number" data-type="minus" data-field="desiredHeight">
					                  <span class="glyphicon glyphicon-minus"></span>
					              </button>
					          </span>
  							</div><br/>


							<div class="input-group">
    						<span class="input-group-addon"><i class="glyphicon glyphicon-resize-horizontal"></i> &nbsp;Desired width&nbsp; [px]</span>
    						<input name="desiredWidth" id="desiredWidth" type="text" class="form-control input-number" onchange="heightKeepRatio();">
    						 <span class="input-group-btn">
					              <button  type="button" class="btn btn-default btn-number" data-type="plus" data-field="desiredWidth">
					                  <span class="glyphicon glyphicon-plus"></span>
					              </button>
					          </span>
    						 <span class="input-group-btn">
					              <button  type="button" class="btn btn-default btn-number" data-type="minus" data-field="desiredWidth">
					                  <span class="glyphicon glyphicon-minus"></span>
					              </button>
					          </span>
  							</div>


						</div>


				</form>
				<br />
				<br />
				<div id="showModified" class="col-md-12" style="display: none;margin-left: auto;margin-right: auto;text-align: center;"><br /><br /> <a href="#" onclick="showImageModified();">[ Show modified image ]</a> </div>
				<div id="imageModified" class="col-md-12" style="display: none;margin-left: auto;margin-right: auto;text-align: center;" > </div>
				<div id="imageModifiedDownload" class="col-md-12" style="display: none;margin-left: auto;margin-right: auto;text-align: center;" ><a href="#" onclick="prepHref(this)" class="button" id="btn-download" download="myMoidifiedImage.jpg">Download</a> </div>
				
				
				<br />
				<br />
				<br />
				



				<div class="col-md-12"><br /><br />
<?php
    $json=file_get_contents("http://localhost:8080/getAll");
    $data =  json_decode($json);

    if (count($data)) {
        
		echo "<h4>&nbsp;Upload history</h4>";
		echo "<div class=\"list-group\">"; 
        
        foreach ($data as $idx => $row) {

        		$id = $row->id;
        		$name = $row->name;
				$date = date("d-m-Y H:i", ($row->date / 1000) );
          
		echo 	'<div href="http://localhost:8080//getImage//'.$id.'" class="list-group-item list-group-item-success">
					<span class="badge alert-primary pull-right">'.$date.'</span>'
					.$name.'

					<a class="pull-right" style="padding-right:20px;" href="http://localhost:8080//getImageModified//'.$id.'"><i>[modified]</i></a>
					<a class="pull-right" style="padding-right:10px;"  href="http://localhost:8080//getImage//'.$id.'"><i>[original]</i></a>
					<a class="pull-right deleteLink" style="padding-right:20px; color: #ED2939;" href="http://localhost:8080//delete//'.$id.'"><i>[delete]</i></a>

				</div>'; 

            // echo "<td><img src='data:image/jpeg;base64, $row->image' /></td>";

        }
    }
?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    
        
	</div>
</div>

<script type="text/javascript" src="script.js"></script>
</body>
</html>


